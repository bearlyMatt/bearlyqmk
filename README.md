# bearlyQMK

Matt's QMK-Compatible keymaps!

## bearlyCU7

This is the silly keymap made specifically for bearlyMatt's CapsUnlocked CU7 macro pad. 

This is combined with reaper presets to make an easy audio editing experience

```
    //BASE LAYER//                       //RAISED LAYER//
+------+------+-------+             +------+-------+-------+
|      | Mic  |       |             |      | Reset |       |
|      | Mute |       |             +------+-------+-------+
+------+------+-------+             | RGB  |   UP  | MO(1) |
| FN23 | FN24 | MO(1) |             +------+-------+-------+
+------+------+-------+             | LEFT | DOWN  | RIGHT |
| FN20 | FN21 | FN22  |             +------+-------+-------+
+------+------+-------+
```
